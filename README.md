# coffee-devkit
Development Toolkit

It's meant to be a collection of things to make life easier for me.
The following is planned:
* Vaadin WebUI on a Server
* Features
 * Konfigure & Clone GIT Repos
   * Bitbucket API
 * Manage many Maven-Projects and Modules at once
   * Updating a dependency to the same Version everywhere
   * unifying build profiles/plugins/manegement
 * Building maven projects
   * Fancy display of test results
   * Coverage / Mutation tests
   * Option to insert plugins just for the Build.
